<?php
/**
 * Created by PhpStorm.
 * User: PhanLong
 * Date: 6/20/2016
 * Time: 2:58 AM
 */
if ( !defined( 'ABSPATH' ) ) {
	exit;
}

vc_map( array(
	"name"                    => __( "Deal Today", 'kutetheme' ),
	"base"                    => "deal_today",
	"category"                => __( 'Kute Theme', 'kutetheme' ),
	"description"             => __( "Show tab categories", 'kutetheme' ),
	"show_settings_on_create" => true,
	"params"                  => array(
		array(
			"type"        => "textfield",
			"heading"     => __( "Title", 'kutetheme' ),
			"param_name"  => "title",
			"admin_label" => true,
		),
		array(
			"type"        => "textfield",
			"heading"     => __( "Ids", 'kutetheme' ),
			"param_name"  => "ids",
			"admin_label" => true,
			"description" => __( "Get product by list ids.( Input IDs which separated by a comma ',' )", 'kutetheme' ),
		),
		array(
			'type'        => 'dropdown',
			'heading'     => __( 'CSS Animation', 'js_composer' ),
			'param_name'  => 'css_animation',
			'admin_label' => false,
			'value'       => array(
				__( 'No', 'js_composer' )                 => '',
				__( 'Top to bottom', 'js_composer' )      => 'top-to-bottom',
				__( 'Bottom to top', 'js_composer' )      => 'bottom-to-top',
				__( 'Left to right', 'js_composer' )      => 'left-to-right',
				__( 'Right to left', 'js_composer' )      => 'right-to-left',
				__( 'Appear from center', 'js_composer' ) => "appear"
			),
			'description' => __( 'Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', 'js_composer' )
		),

		array(
			'type'        => 'css_editor',
			'heading'     => __( 'Css', 'js_composer' ),
			'param_name'  => 'css',
			'group'       => __( 'Design options', 'js_composer' ),
			'admin_label' => false,
		),

	),
) );

class WPBakeryShortCode_Deal_Today extends WPBakeryShortCode {
	public $product_size = 'kt_shop_catalog_214';

	protected function content( $atts, $content = null ) {
		$atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'categories_tab', $atts ) : $atts;
		extract( shortcode_atts( array(
			'title'         => 'Tabs Name',
			'ids'          => '',
			'css_animation' => '',
			'el_class'      => '',
			'css'           => '',
		), $atts ) );

		global $woocommerce_loop;

		$elementClass = array(
			'base'             => apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, ' box-tab-category ', $this->settings['base'], $atts ),
			'extra'            => $this->getExtraClass( $el_class ),
			'css_animation'    => $this->getCSSAnimation( $css_animation ),
			'shortcode_custom' => vc_shortcode_custom_css_class( $css, ' ' )
		);

		$elementClass = preg_replace( array( '/\s+/', '/^\s|\s$/' ), array( ' ', '' ), implode( ' ', $elementClass ) );
		ob_start();
		$meta_query = WC()->query->get_meta_query();

		$args     = array(
			'post_type'           => 'product',
			'post_status'         => 'publish',
			'ignore_sticky_posts' => 1,
			'posts_per_page'      => 3,
			'meta_query'          => $meta_query,
			'suppress_filter'     => true,
			'post__in'            => $ids,
			'orderby'             => 'post__in'
		);
		$products = new WP_Query( apply_filters( 'woocommerce_shortcode_products_query', $args, $atts ) );

		if ( $products->have_posts() ) :
			?>
			<section id="deal-today" class="deal-today <?php echo $elementClass ?>">
				<div class="entry-header">
					<h2><?php echo esc_html($title) ?></h2>
				</div>
				<div class="deal-today-wrapper">
					<div class="row">
						<?php
						$key = 0;
						while ( $products->have_posts() ) : $products->the_post();
							global $product;
							$key ++;
							$thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), "full" );
							$percentage    = round( ( ( $product->regular_price - $product->sale_price ) / $product->regular_price ) * 100 );
							if ( $key == 1 ) :
								$img = '';
								$resize    = matthewruddy_image_resize( $thumbnail_src[0], 800, 400 );
								if ( $resize != null ) {
									$img = $resize['url'];
								}
								?>
								<div class="col-md-8 col-sm-6 col-xs-12">
									<div class="first-deal-product">
										<div class="entry-thumnail">
											<a href="<?php the_permalink() ?>">
												<img src="<?php echo $img ?>" alt="<?php echo get_the_title( $post->ID ) ?>">
											</a>
											<div class="deal-discount">
												<?php
												echo '<div class="deal-discount-text">Giảm</div>';
												echo '<div class="deal-discount-number">' . $percentage . '%</div>';
												?>
											</div>
										</div>
										<div class="entry-content row ">
											<div class="deal-info-panel left col-sm-7">
												<div class="entry-title">
													<h3 class="product-name">
														<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

												</div>
												<div class="deal-price">
													<?php
													/**
													 * woocommerce_after_shop_loop_item_title hook
													 * @hooked woocommerce_template_loop_price - 5
													 * @hooked woocommerce_template_loop_rating - 10
													 */
													do_action( 'kt_after_shop_loop_item_title' );
													?>
												</div>
												<div class="show-count-down">
													<?php
													$time = kt_get_max_date_sale( get_the_ID() );

													$y = date( 'Y', $time );
													$m = date( 'm', $time );
													$d = date( 'd', $time );
													?>

												</div>
											</div>
											<div class="deal-info-panel right col-sm-5">
												<span class="countdown-lastest count-down-time" data-y="<?php echo esc_attr( $y ); ?>" data-m="<?php echo esc_attr( $m ); ?>" data-d="<?php echo esc_attr( $d ); ?>" data-h="00" data-i="00" data-s="00"></span>
												<?php
												woocommerce_template_loop_add_to_cart();
												?>
											</div>
										</div>
									</div>
								</div>
							<?php else :
								$img = '';
								$resize    = matthewruddy_image_resize( $thumbnail_src[0], 200, 200 );
								if ( $resize != null ) {
									$img = $resize['url'];
								}
								?>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="sale-today-item">
										<div class="row">
											<div class="deal-info-panel left col-md-5 col-sm-4">
												<div class="entry-thumnail">
													<a href="<?php the_permalink() ?>">
														<img src="<?php echo $img ?>" alt="<?php echo get_the_title( $post->ID ) ?>">
													</a>
													<div class="deal-discount">
														<?php
														echo '<div class="deal-discount-text">Giảm</div>';
														echo '<div class="deal-discount-number">' . $percentage . '%</div>';
														?>
													</div>
												</div>
											</div>
											<div class="deal-info-panel right col-md-7 col-sm-6">
												<div class="entry-content">
													<div class="deal-info-panel">
														<div class="entry-title">
															<h3 class="product-name">
																<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

														</div>
														<div class="deal-price">
															<?php
															/**
															 * woocommerce_after_shop_loop_item_title hook
															 * @hooked woocommerce_template_loop_price - 5
															 * @hooked woocommerce_template_loop_rating - 10
															 */
															do_action( 'kt_after_shop_loop_item_title' );
															?>
														</div>
														<div class="show-count-down">
															<?php
															$time = kt_get_max_date_sale( get_the_ID() );

															$y = date( 'Y', $time );
															$m = date( 'm', $time );
															$d = date( 'd', $time );
															?>

														</div>
													</div>
													<div class="deal-info-panel">
														<span class="countdown-lastest count-down-time" data-y="<?php echo esc_attr( $y ); ?>" data-m="<?php echo esc_attr( $m ); ?>" data-d="<?php echo esc_attr( $d ); ?>" data-h="00" data-i="00" data-s="00"></span>
														<?php
														woocommerce_template_loop_add_to_cart();
														?>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

								<?php
							endif;
						endwhile; ?>
					</div>
				</div>
			</section>
			<?php
		endif;

		return ob_get_clean();
	}
}