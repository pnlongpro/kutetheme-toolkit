<?php
/**
 * Created by PhpStorm.
 * User: PhanLong
 * Date: 6/13/2016
 * Time: 2:20 PM
 */
if ( !defined( 'ABSPATH' ) ) {
	exit;
}
vc_map( array(
	"name"                    => __( "Projects", 'kutetheme' ),
	"base"                    => "projects",
	"category"                => __( 'Kute Theme', 'kutetheme' ),
	"description"             => __( "Show projects", 'kutetheme' ),
	"show_settings_on_create" => true,
	"params"                  => array(
		array(
			"type"        => "textfield",
			"heading"     => __( "Title", 'kutetheme' ),
			"param_name"  => "title",
			"admin_label" => true,
		),
		array(
			"type"        => "textfield",
			"heading"     => __( "Number Post", 'kutetheme' ),
			"param_name"  => "per_page",
			'std'         => 10,
			"admin_label" => false,
			'description' => __( 'Number post in a slide', 'kutetheme' )
		),
		// Carousel
		array(
			'type'        => 'dropdown',
			'value'       => array(
				__( 'Yes', 'js_composer' ) => 'true',
				__( 'No', 'js_composer' )  => 'false'
			),
			'std'         => 'false',
			'heading'     => __( 'AutoPlay', 'kutetheme' ),
			'param_name'  => 'autoplay',
			'group'       => __( 'Carousel settings', 'kutetheme' ),
			'admin_label' => false
		),
		array(
			'type'        => 'dropdown',
			'value'       => array(
				__( 'Yes', 'js_composer' ) => 'true',
				__( 'No', 'js_composer' )  => 'false'
			),
			'std'         => 'false',
			'heading'     => __( 'Navigation', 'kutetheme' ),
			'param_name'  => 'navigation',
			'description' => __( "Show buton 'next' and 'prev' buttons.", 'kutetheme' ),
			'group'       => __( 'Carousel settings', 'kutetheme' ),
			'admin_label' => false,
		),
		array(
			'type'        => 'dropdown',
			'value'       => array(
				__( 'Yes', 'js_composer' ) => 'true',
				__( 'No', 'js_composer' )  => 'false'
			),
			'std'         => 'false',
			'heading'     => __( 'Loop', 'kutetheme' ),
			'param_name'  => 'loop',
			'description' => __( "Inifnity loop. Duplicate last and first items to get loop illusion.", 'kutetheme' ),
			'group'       => __( 'Carousel settings', 'kutetheme' ),
			'admin_label' => false,
		),
		array(
			"type"        => "kt_number",
			"heading"     => __( "Slide Speed", 'kutetheme' ),
			"param_name"  => "slidespeed",
			"value"       => "250",
			"suffix"      => __( "milliseconds", 'kutetheme' ),
			"description" => __( 'Slide speed in milliseconds', 'kutetheme' ),
			'group'       => __( 'Carousel settings', 'kutetheme' ),
			'admin_label' => false,
		),
		array(
			"type"        => "kt_number",
			"heading"     => __( "Margin", 'kutetheme' ),
			"param_name"  => "margin",
			"value"       => "0",
			"suffix"      => __( "px", 'kutetheme' ),
			"description" => __( 'Distance( or space) between 2 item', 'kutetheme' ),
			'group'       => __( 'Carousel settings', 'kutetheme' ),
			'admin_label' => false,
		),
		array(
			'type'        => 'dropdown',
			'value'       => array(
				__( 'Yes', 'js_composer' ) => 1,
				__( 'No', 'js_composer' )  => 0
			),
			'std'         => 1,
			'heading'     => __( 'Use Carousel Responsive', 'kutetheme' ),
			'param_name'  => 'use_responsive',
			'description' => __( "Try changing your browser width to see what happens with Items and Navigations", 'kutetheme' ),
			'group'       => __( 'Carousel responsive', 'kutetheme' ),
			'admin_label' => false,
		),
		array(
			"type"        => "kt_number",
			"heading"     => __( "The items on destop (Screen resolution of device >= 992px )", 'kutetheme' ),
			"param_name"  => "items_destop",
			"value"       => "5",
			"suffix"      => __( "item", 'kutetheme' ),
			"description" => __( 'The number of items on destop', 'kutetheme' ),
			'group'       => __( 'Carousel responsive', 'kutetheme' ),
			'admin_label' => false,
		),
		array(
			"type"        => "kt_number",
			"heading"     => __( "The items on tablet (Screen resolution of device >=768px and < 992px )", 'kutetheme' ),
			"param_name"  => "items_tablet",
			"value"       => "3",
			"suffix"      => __( "item", 'kutetheme' ),
			"description" => __( 'The number of items on destop', 'kutetheme' ),
			'group'       => __( 'Carousel responsive', 'kutetheme' ),
			'admin_label' => false,
		),
		array(
			"type"        => "kt_number",
			"heading"     => __( "The items on mobile (Screen resolution of device < 768px)", 'kutetheme' ),
			"param_name"  => "items_mobile",
			"value"       => "1",
			"suffix"      => __( "item", 'kutetheme' ),
			"description" => __( 'The numbers of item on destop', 'kutetheme' ),
			'group'       => __( 'Carousel responsive', 'kutetheme' ),
			'admin_label' => false,
		),
		array(
			'type'        => 'css_editor',
			'heading'     => __( 'Css', 'js_composer' ),
			'param_name'  => 'css',
			// 'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' ),
			'group'       => __( 'Design options', 'js_composer' ),
			'admin_label' => false,
		),
	)
) );
if ( !class_exists( 'WPBakeryShortCode_Projects' ) ) :
	class WPBakeryShortCode_Projects extends WPBakeryShortCode {

		protected function content( $atts, $content = null ) {
			$atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'projects', $atts ) : $atts;
			extract( shortcode_atts( array(
				'title'          => __( 'From the blog', 'kutetheme' ),
				'subtitle'       => '',
				'per_page'       => 10,
				'autoplay'       => 'false',
				'navigation'     => 'false',
				'margin'         => 0,
				'slidespeed'     => 250,
				'css'            => '',
				'css_animation'  => '',
				'el_class'       => '',
				'loop'           => 'true',
				//Default
				'use_responsive' => 1,
				'items_destop'   => 5,
				'items_tablet'   => 3,
				'items_mobile'   => 1,
			), $atts ) );
			$elementClass = array(
				'base'             => apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, '', $this->settings['base'], $atts ),
				'extra'            => $this->getExtraClass( $el_class ),
				'css_animation'    => $this->getCSSAnimation( $css_animation ),
				'shortcode_custom' => vc_shortcode_custom_css_class( $css, ' ' )
			);

			$elementClass = preg_replace( array( '/\s+/', '/^\s|\s$/' ), array( ' ', '' ), implode( ' ', $elementClass ) );

			$data_carousel = array(
				"autoplay"           => $autoplay,
				"nav"                => $navigation,
				"margin"             => $margin,
				"smartSpeed"         => $slidespeed,
				"theme"              => 'style-navigation-bottom',
				"autoheight"         => 'false',
				'dots'               => 'false',
				'loop'               => $loop,
				'autoplayTimeout'    => 1000,
				'autoplayHoverPause' => 'true'
			);


			$args  = array(
				'post_type'           => 'projects',
				'post_status'         => 'publish',
				'ignore_sticky_posts' => 1,
				'posts_per_page'      => $per_page,
				'suppress_filter'     => true,
			);
			$posts = new WP_Query( apply_filters( 'woocommerce_shortcode_products_query', $args, $atts ) );
			ob_start();
			global $post;
			if ( $posts->have_posts() ):
				if ( $posts->post_count <= 1 ) {
					$data_carousel['loop'] = 'false';
				}

				if ( $use_responsive ) {
					$arr                         = array(
						'0'   => array(
							"items" => $items_mobile
						),
						'768' => array(
							"items" => $items_tablet
						),
						'992' => array(
							"items" => $items_destop
						)
					);
					$data_responsive             = json_encode( $arr );
					$data_carousel["responsive"] = $data_responsive;
				} else {
					$data_carousel['items'] = $items_destop;

				}

				?>
				<div class="project-wrapper">
					<div class="entry-header">
						<?php $title = $title != '' ? $title : 'Projects'; ?>
						<span><a href="http://camerauytin.vn/du-an/"><?php echo esc_html( $title ) ?></a></span>
					</div>
					<div class="entry-content">
						<div class="owl-carousel" <?php echo _data_carousel( $data_carousel ); ?>>
							<?php while ( $posts->have_posts() ): $posts->the_post(); ?>
								<div class="project-items">
									<?php if( has_post_thumbnail() ): ?>
										<div class="post-thumb image-hover2">
											<a href="<?php the_permalink() ?>">
												<?php
												$thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), "kt_post_blog_268x255" );
												if( $thumbnail_src ):?>
													<img width="<?php echo esc_attr( $thumbnail_src[1])?>" height="<?php echo esc_attr( $thumbnail_src[2])?>" alt="<?php the_title() ?>" class="owl-lazy attachment-post-thumbnail wp-post-image" src="<?php echo esc_url( $temping_post_thumbnail ); ?>" data-src="<?php echo esc_url( $thumbnail_src[0] ) ?>" />
												<?php else: ?>
													<img width="<?php echo esc_attr( $thumbnail_src[1])?>" height="<?php echo esc_attr( $thumbnail_src[2])?>" alt="<?php the_title() ?>" class="owl-lazy attachment-post-thumbnail wp-post-image" src="<?php echo esc_url( $temping_post_thumbnail ) ?>" />
												<?php endif; ?>
											</a>

										</div>
										<h3 class="entry-title"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h3>
									<?php endif; ?>
								</div>
							<?php endwhile; ?>
						</div>
					</div>
				</div><!--end project-wrapper-->
				<?php
			endif;
			$result = ob_get_clean();

			return $result;
		}

	}
endif;