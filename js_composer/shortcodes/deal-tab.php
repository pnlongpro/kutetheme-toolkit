<?php
// Exit if accessed directly
if ( !defined('ABSPATH')) exit;

vc_map( array(
	"name"                    => __( "Deal Tab", 'kutetheme'),
	"base"                    => "deal_tab",
	"category"                => __('Kute Theme', 'kutetheme' ),
	"description"             => __( "Show tab hot deal", 'kutetheme'),
	"as_parent"               => array('only' => 'tab_sections'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
	"content_element"         => true,
	"show_settings_on_create" => true,
	"params"                  => array(
		array(
			"type"        => "textfield",
			"heading"     => __( "Title", 'kutetheme' ),
			"param_name"  => "title",
			"admin_label" => true,
		),
		array(
			"type"        => "textfield",
			"heading"     => __( "Number Post", 'kutetheme' ),
			"param_name"  => "per_page",
			'std'         => 5,
			"admin_label" => false,
			'description' => __( 'Number post in a slide', 'kutetheme' )
		),
		array(
			"type"       => "dropdown",
			"heading"    => __("Order by", 'kutetheme'),
			"param_name" => "orderby",
			"value"      => array(
				__('None', 'kutetheme')     => 'none',
				__('ID', 'kutetheme')       => 'ID',
				__('Author', 'kutetheme')   => 'author',
				__('Name', 'kutetheme')     => 'name',
				__('Date', 'kutetheme')     => 'date',
				__('Modified', 'kutetheme') => 'modified',
				__('Rand', 'kutetheme')     => 'rand',
			),
			'std'         => 'date',
			"description" => __("Select how to sort retrieved posts.",'kutetheme')
		),
		array(
			"type"       => "dropdown",
			"heading"    => __("Order", 'kutetheme'),
			"param_name" => "order",
			"value"      => array(
				__('ASC', 'kutetheme')  => 'ASC',
				__('DESC', 'kutetheme') => 'DESC'
			),
			'std'         => 'DESC',
			"description" => __( "Designates the ascending or descending order.", 'kutetheme' )
		),
		array(
			'type'        => 'dropdown',
			'heading'     => __( 'CSS Animation', 'js_composer' ),
			'param_name'  => 'css_animation',
			'admin_label' => false,
			'value'       => array(
				__( 'No', 'js_composer' ) => '',
				__( 'Top to bottom', 'js_composer' ) => 'top-to-bottom',
				__( 'Bottom to top', 'js_composer' ) => 'bottom-to-top',
				__( 'Left to right', 'js_composer' ) => 'left-to-right',
				__( 'Right to left', 'js_composer' ) => 'right-to-left',
				__( 'Appear from center', 'js_composer' ) => "appear"
			),
			'description' => __( 'Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', 'js_composer' )
		),
		array(
			'type'        => 'css_editor',
			'heading'     => __( 'Css', 'js_composer' ),
			'param_name'  => 'css',
			'group'       => __( 'Design options', 'js_composer' ),
			'admin_label' => false,
		),
		array(
			"type"        => "textfield",
			"heading"     => __( "Extra class name", "js_composer" ),
			"param_name"  => "el_class",
			"description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "js_composer" ),
			'admin_label' => false,
		),
	),
	"js_view" => 'VcColumnView'
));
vc_map( array(
	"name"            => __("Section Tab", 'kutetheme'),
	"base"            => "tab_sections",
	"content_element" => true,
	"as_child"        => array('only' => 'deal_tab'), // Use only|except attributes to limit parent (separate multiple values with comma)
	"params"          => array(
		// add params same as with any other content element
		array(
			"type"        => "textfield",
			"heading"     => __( "Header", 'kutetheme' ),
			"param_name"  => "header",
			"admin_label" => true,
		),
		array(
			"type"        => "textfield",
			"heading"     => __( "Ids", 'kutetheme' ),
			"param_name"  => "ids",
			"admin_label" => true,
			"description" => __( "Get product by list ids.( Input IDs which separated by a comma ',' )", 'kutetheme' ),
		),
	)
) );
class WPBakeryShortCode_Deal_Tab extends WPBakeryShortCodesContainer {

	protected function content($atts, $content = null) {
		$atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'hot_deal', $atts ) : $atts;
		extract( shortcode_atts( array(
			'title'          => 'Tabs Name',
			'size'           => 'kt_shop_catalog_164',
			'per_page'       => 5,
			'orderby'        => 'date',
			'order'          => 'DESC',
			'css'            => '',
			'el_class'       => '',
			'css_animation'  => '',
		), $atts ) );

		global $woocommerce_loop;

		$elementClass = array(
			'base'             => apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, ' box-tab-category ', $this->settings['base'], $atts ),
			'extra'            => $this->getExtraClass( $el_class ),
			'css_animation'    => $this->getCSSAnimation( $css_animation ),
			'shortcode_custom' => vc_shortcode_custom_css_class( $css, ' ' )
		);

		$elementClass = preg_replace( array( '/\s+/', '/^\s|\s$/' ), array( ' ', '' ), implode( ' ', $elementClass ) );

		$meta_query = WC()->query->get_meta_query();

		$args = array(
			'post_type'			  => 'product',
			'post_status'		  => 'publish',
			'ignore_sticky_posts' => 1,
			'posts_per_page' 	  => $per_page,
			'suppress_filter'     => true,
			'orderby'             => $orderby,
			'order'               => $order
		);
		$tabs = kt_get_all_attributes( 'tab_sections', $content );
		if( count( $tabs ) >0 ) :
			$unique = uniqid();
			?>
			<?php ob_start(); ?>
			<div class="deal-tab-wrapper">
				<!-- Hot deals -->
				<div class="entry-header">
					<h2><?php echo $title; ?></h2>
				</div>
				<div class="hot-deals-row container-tab">
					<div class="hot-deals-box only_countdown">
						<div class="row">
							<div class="col-md-12">
								<div class="hot-deals-tab">
									<div class="hot-deals-tab-box">
										<ul class="nav-tab">
											<?php $i = 1; ?>
											<?php foreach( $tabs as $tab ):
												extract( shortcode_atts( array(
													'header'         => __( 'Tab name', 'kutetheme' ),
												), $tab ) );
												?>
												<li <?php if( $i ==1 ): ?> class="active" <?php endif; ?> ><a data-toggle="tab" href="#hotdeals-<?php echo $unique ?>-<?php echo $i; ?>"><?php echo $header; ?></a></li>
												<?php $i++; ?>
											<?php endforeach; ?>
										</ul>
										<div class="box-count-down">
											<span class="countdown-only"></span>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-12 hot-deals-tab-content-col">
								<div class="hot-deals-tab-content tab-container">
									<?php $i = 1; ?>
									<?php
									$max_time = 0;
									foreach( $tabs as $tab ) :
										extract( shortcode_atts( array(
											'header'         => __( 'Tab name', 'kutetheme' ),
											'ids'         => '',
										), $tab ) );
										$ids = explode( ',', $ids );

										$args['post__in'] = $ids;
										$products = new WP_Query( apply_filters( 'woocommerce_shortcode_products_query', $args, $atts ) );

										if( $products->have_posts() ):
											if($products->post_count <=1 ){
												$data_carousel['loop'] = 'false';
											}



											add_filter("woocommerce_get_price_html_from_to", "kt_get_price_html_from_to", 10 , 4);
//											add_filter( 'woocommerce_sale_price_html', 'woocommerce_custom_sales_price', 10, 2 );
											remove_action('kt_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 10);
											add_filter( 'kt_product_thumbnail_loop', array( &$this, 'get_size_product' ) );
											?>
											<div id="hotdeals-<?php echo $unique ?>-<?php echo $i; ?>" class="tab-panel <?php if( $i ==1 ): ?>active<?php endif; ?>">
												<?php do_action( "woocommerce_shortcode_before_hot_deal_loop" ); ?>
												<div class="product-list nav-center row" >
													<?php while( $products->have_posts() ): $products->the_post(); ?>
														<div class="col-md-4 col-sm-6 col-xs-12">
															<?php
															wc_get_template_part( 'content', 'product-deal-tab' );
															// Get date sale
															$time = kt_get_max_date_sale( get_the_ID() );
															if( $time > $max_time ){
																$max_time = $time;
															}

															?>
														</div>
													<?php endwhile; ?>
												</div>
												<?php do_action( "woocommerce_shortcode_after_hot_deal_loop" ); ?>
											</div>
											<?php
											remove_filter( "woocommerce_get_price_html_from_to", "kt_get_price_html_from_to", 10 , 4);
											remove_filter( 'woocommerce_sale_price_html', 'woocommerce_custom_sales_price', 10, 2 );
											add_action('kt_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 10);
											remove_filter( 'kt_product_thumbnail_loop', array( &$this, 'get_size_product' ) );
											?>
										<?php else: ?>
											<div id="hotdeals-<?php echo $unique ?>-<?php echo $i; ?>" class="tab-panel <?php if( $i ==1 ): ?>active<?php endif; ?>">
												<label><?php _e( 'Empty product', 'kutetheme' ) ?></label>
											</div>
										<?php endif; ?>
										<?php
										wp_reset_query();
										wp_reset_postdata();
										?>
										<?php $i++; ?>
									<?php endforeach; ?>
									<?php
									if( $max_time > 0 ){
										$y = date( 'Y', $max_time );
										$m = date( 'm', $max_time );
										$d = date( 'd', $max_time );
										?>
										<input class="max-time-sale" data-y="<?php echo esc_attr( $y );?>" data-m="<?php echo esc_attr( $m );?>" data-d="<?php echo esc_attr( $d );?>" type="hidden" value="" />
										<?php

									}
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- ./Hot deals -->
			<?php
		endif;
		$result = ob_get_contents();
		ob_end_clean();
		return $result;
	}
	public function get_size_product( $size ){
		return $this->product_size;
	}
}